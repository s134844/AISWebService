package org.padawanconsulting.web.types;

import dk.dma.ais.message.AisMessage;

/**
 * Created by Oliver on 04-12-2016.
 */
public class ParsedMessageWithTimeStamp {
    private AisMessage aisMessage;
    private String timeStamp;

    public ParsedMessageWithTimeStamp(AisMessage aisMessage, String timeStamp) {
        this.aisMessage = aisMessage;
        this.timeStamp = timeStamp;
    }

    public AisMessage getAisMessage() {
        return aisMessage;
    }

    public void setAisMessage(AisMessage aisMessage) {
        this.aisMessage = aisMessage;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
