package org.padawanconsulting.web.types;

/**
 * Created by Oliver on 02-12-2016.
 */
public class MessageWithTimeStamp {

    private byte[] message;
    private String timeStamp;

    public byte[] getMessage() {
        return message;
    }

    public void setMessage(byte[] message) {
        this.message = message;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
